package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
 
import model.Produit;

public class DAOProduit extends DAOGeneric<Produit>{
	
	public DAOProduit(Session session) {
		super(session, Produit.class);
	}
	

}
