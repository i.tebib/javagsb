package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import model.User;

public class DAOConnexion extends DAOGeneric<User>{
	
	public DAOConnexion(Session session) {
		super(session, User.class);
	}

	public User verifmail(String email) {
		String SQL = "SELECT * FROM User WHERE email=:email";
		SQLQuery query = session.createSQLQuery(SQL);
		query.setString("email", email);
		query.addEntity(entityClas);
		User user = (User) query.uniqueResult();
		return user;
	}
	
	public User findByName(String name) {
		String SQL = "SELECT * FROM User WHERE name=:name";
		SQLQuery query = session.createSQLQuery(SQL);
		query.setString("name", name);
		query.addEntity(entityClas);
		User user = (User) query.uniqueResult();
		return user;
	}
}











