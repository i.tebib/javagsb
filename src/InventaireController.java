import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JButton;

import DAO.DAOCategorie;
import DAO.DAOCommande;
import DAO.DAOConnexion;
import DAO.DAOProduit;
import model.Produit;
import model.User;
import util.HibernateUtil;
import model.Commande;
import View.AddProduit;
import View.AfficherCommande;
import View.AfficherProduit;
import View.AfficherProduitsAdmin;
import View.GestionStock;
import View.Inventaire;

public class InventaireController implements ActionListener {
	
	private Inventaire fenetre;
	DAOCommande daoCommande;
	List<Commande> commandes;
	List<Produit> produits;
	MyDefaultModelInventaire myDTMI;

	public InventaireController(Inventaire f, DAOCommande daoCommande) {
		this.fenetre = f;
		this.daoCommande = daoCommande;	
		
		fenetre.getBtnRetour().addActionListener(this);
		
	}
	
	public void init() {
		commandes = daoCommande.findAll();
		fenetre.setVisible(true);
		myDTMI = new MyDefaultModelInventaire(commandes);
		fenetre.getTable().setModel(myDTMI);
	}
	
	public void Retour() {
		fenetre.setVisible(false);
		ProduitControlleradmin count = new ProduitControlleradmin(new AfficherProduitsAdmin(), new AddProduit(), new DAOProduit(HibernateUtil.getSessionFactory().openSession()), new DAOCategorie(HibernateUtil.getSessionFactory().openSession()));
		count.init();
	}
	
	public void ProdVendu() {
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton)e.getSource();
		switch (btn.getName()) {
		case "Retour":
			Retour();
			break;
			
		default:
			break;
		}
		
	}

}
