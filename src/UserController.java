import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import DAO.DAOCategorie;
import DAO.DAOConnexion;
import DAO.DAOProduit;
import View.AddProduit;
import View.AfficherProduit;
import View.AfficherProduitsAdmin;
import View.Connexion;
import model.User;
import util.HibernateUtil;

public class UserController implements ActionListener{
	
	private Connexion fenetre;
	DAOConnexion daoConnexion;

	public UserController(Connexion f, DAOConnexion daoConnexion) {
		this.fenetre = f;
		this.daoConnexion = daoConnexion;
		
		fenetre.getBtnConnexion().addActionListener(this);
		
	}
	
	public void init() {
		
		fenetre.setVisible(true);
	}
	
	public void Connexion() {
		
		String erreur = "L'email ou le mot de passe est incorrecte";
		String email = fenetre.getConnectionField().getText();
		User user = daoConnexion.verifmail(email);
		int role = Integer.parseInt(user.getRoles());
		
		if(user==null) {
			fenetre.getLblErreur().setText(erreur);
		}
		else {
			if(user.getPassword().equals(fenetre.getPasswordFielmdp().getText())) {
				if (role == 1) {
					fenetre.setVisible(false);
					ProduitControlleradmin count = new ProduitControlleradmin(new AfficherProduitsAdmin(), new AddProduit(), new DAOProduit(HibernateUtil.getSessionFactory().openSession()), new DAOCategorie(HibernateUtil.getSessionFactory().openSession()));
					count.init();
				}
				else {
					fenetre.setVisible(false);
					ProduitController count = new ProduitController(new AfficherProduit(), new AddProduit(), new DAOProduit(HibernateUtil.getSessionFactory().openSession()), new DAOCategorie(HibernateUtil.getSessionFactory().openSession()));
					count.init();
				}
			}
			
		}
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton)e.getSource();
		switch (btn.getName()) {
		case "Connexion":
			Connexion();
			break;
			
		default:
			break;
		}
		
	}
	
}
