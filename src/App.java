
import java.util.List;

import javax.swing.JOptionPane;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.mysql.cj.util.TestUtils;

import DAO.DAOConnexion;
import model.Categorie;
import model.Produit;
import model.User;
import model.Permission;
import util.HibernateUtil;
import util.TestConnection;
import View.Connexion;

public class App {

	
	
	public static void main(String[] args) {
		System.out.println("Starting");
		try {
			UserController count = new UserController(new Connexion(), new DAOConnexion(HibernateUtil.getSessionFactory().openSession()));
			count.init();
		} 
		catch (Exception e) {
			JOptionPane d = new JOptionPane();
			d.showMessageDialog(null, "Probleme de connexion a la BDD", "fail", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			
			System.exit(4);	
		}
	}

}
