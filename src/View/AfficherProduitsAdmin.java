package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class AfficherProduitsAdmin extends JFrame {

	private JPanel contentPane;
	private JButton btnInventaire;
	private JButton btnGestionStock;
	private JButton btnAjouter;
	private JButton btnAfficherCommande;
	private JButton btnUpdate;
	private JButton btnDelete;
	private JScrollPane scrollPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AfficherProduitsAdmin frame = new AfficherProduitsAdmin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AfficherProduitsAdmin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 504, 346);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel contentPane_1 = new JPanel();
		contentPane_1.setLayout(null);
		contentPane_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane_1.setBounds(0, 0, 470, 306);
		contentPane.add(contentPane_1);
		
		JLabel Label1 = new JLabel("Liste des Produits");
		Label1.setHorizontalAlignment(SwingConstants.LEFT);
		Label1.setBounds(5, 5, 430, 15);
		contentPane_1.add(Label1);
		
		btnAfficherCommande = new JButton("Afficher les commande");
		btnAfficherCommande.setName("AfficherCommande");
		btnAfficherCommande.setBounds(201, 63, 203, 25);
		contentPane_1.add(btnAfficherCommande);
		
		btnAjouter = new JButton("Ajouter un produit");
		btnAjouter.setName("AjouterProduit");
		btnAjouter.setBounds(12, 63, 164, 25);
		contentPane_1.add(btnAjouter);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 107, 413, 147);
		contentPane_1.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"New column", "New column", "New column", "New column"
			}
		));
		scrollPane.setViewportView(table);
		
		btnGestionStock = new JButton("Gestion des stock");
		btnGestionStock.setName("GestionStock");
		btnGestionStock.setBounds(15, 26, 213, 25);
		contentPane_1.add(btnGestionStock);
		
		btnUpdate = new JButton("Update");
		btnUpdate.setName("Update");
		btnUpdate.setBounds(12, 269, 117, 25);
		contentPane_1.add(btnUpdate);
		
		btnDelete = new JButton("Delete");
		btnDelete.setName("Delete");
		btnDelete.setBounds(318, 266, 117, 25);
		contentPane_1.add(btnDelete);
		
		btnInventaire = new JButton("Inventaire");
		btnInventaire.setName("inventaire");
		btnInventaire.setBounds(240, 26, 164, 25);
		contentPane_1.add(btnInventaire);
	}
	public JButton getBtnInventaire() {
		return btnInventaire;
	}
	public JButton getBtnGestionStock() {
		return btnGestionStock;
	}
	public JButton getBtnAjouter() {
		return btnAjouter;
	}
	public JButton getBtnAfficherCommande() {
		return btnAfficherCommande;
	}
	public JButton getBtnUpdate() {
		return btnUpdate;
	}
	public JButton getBtnDelete() {
		return btnDelete;
	}
	public JScrollPane getScrollPane() {
		return scrollPane;
	}
	public JTable getTable() {
		return table;
	}
}
