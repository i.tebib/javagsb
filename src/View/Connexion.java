package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

public class Connexion extends JFrame {

	private JPanel contentPane;
	private JTextField ConnectionField;
	private JLabel lblMotDePasse;
	private JLabel lblUtilisateur;
	private JButton btnConnexion;
	private JLabel lblErreur;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Connexion frame = new Connexion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Connexion() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblUtilisateur = new JLabel("utilisateur :");
		lblUtilisateur.setBounds(50, 41, 101, 15);
		contentPane.add(lblUtilisateur);
		
		ConnectionField = new JTextField();
		ConnectionField.setBounds(169, 39, 141, 19);
		contentPane.add(ConnectionField);
		ConnectionField.setColumns(10);
		
		lblMotDePasse = new JLabel("Mot de passe :");
		lblMotDePasse.setBounds(35, 105, 116, 15);
		contentPane.add(lblMotDePasse);
		
		btnConnexion = new JButton("Connexion");
		btnConnexion.setName("Connexion");
		btnConnexion.setBounds(136, 198, 117, 25);
		contentPane.add(btnConnexion);
		
		lblErreur = new JLabel("");
		lblErreur.setBounds(12, 155, 416, 15);
		contentPane.add(lblErreur);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(169, 103, 141, 19);
		contentPane.add(passwordField);
	}
	public JLabel getLblMotDePasse() {
		return lblMotDePasse;
	}
	public JLabel getLblUtilisateur() {
		return lblUtilisateur;
	}
	public JTextField getConnectionField() {
		return ConnectionField;
	}
	public JButton getBtnConnexion() {
		return btnConnexion;
	}
	public JLabel getLblErreur() {
		return lblErreur;
	}
	public JPasswordField getPasswordFielmdp() {
		return passwordField;
	}
}
