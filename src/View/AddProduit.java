package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.AbstractButton;
import javax.swing.JButton;

public class AddProduit extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNomProduit;
	private JTextField textFieldQuantite;
	private JTextField textFieldCategorie;
	private JButton btnConfirmer;
	private JButton btnRetour;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddProduit frame = new AddProduit();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddProduit() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomDuProduit = new JLabel("Nom du produit");
		lblNomDuProduit.setBounds(28, 12, 114, 15);
		contentPane.add(lblNomDuProduit);
		
		textFieldNomProduit = new JTextField();
		textFieldNomProduit.setBounds(176, 10, 218, 19);
		contentPane.add(textFieldNomProduit);
		textFieldNomProduit.setColumns(10);
		
		JLabel lblQuantit = new JLabel("Quantité");
		lblQuantit.setBounds(28, 68, 70, 15);
		contentPane.add(lblQuantit);
		
		textFieldQuantite = new JTextField();
		textFieldQuantite.setBounds(176, 66, 114, 19);
		contentPane.add(textFieldQuantite);
		textFieldQuantite.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Categorie Id");
		lblNewLabel.setBounds(28, 137, 114, 15);
		contentPane.add(lblNewLabel);
		
		textFieldCategorie = new JTextField();
		textFieldCategorie.setBounds(176, 135, 114, 19);
		contentPane.add(textFieldCategorie);
		textFieldCategorie.setColumns(10);
		
		btnConfirmer = new JButton("Confirmer");
		btnConfirmer.setName("Confirmer");
		btnConfirmer.setBounds(28, 201, 117, 25);
		contentPane.add(btnConfirmer);
		
		btnRetour = new JButton("Retour");
		btnRetour.setName("Retour");
		btnRetour.setBounds(277, 201, 117, 25);
		contentPane.add(btnRetour);
	}
	public JButton getBtnConfirme() {
		return btnConfirmer;
	}
	public JButton getBtnRetour() {
		return btnRetour;
	}
	public JTextField getTextFieldCategorie() {
		return textFieldCategorie;
	}
	public JTextField getTextFieldQuantite() {
		return textFieldQuantite;
	}
	public JTextField getTextFieldNomProduit() {
		return textFieldNomProduit;
	}

}
