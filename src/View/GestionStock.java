package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GestionStock extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldIdProd;
	private JTextField textFieldStockEntrant;
	private JTextField textFieldStockSortant;
	private JButton btnConfirmer;
	private JButton btnRetour;
	private JLabel lblErreur;
	private JTextField textFieldName;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GestionStock frame = new GestionStock();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GestionStock() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblId = new JLabel("id ");
		lblId.setBounds(12, 12, 70, 15);
		contentPane.add(lblId);
		
		textFieldIdProd = new JTextField();
		textFieldIdProd.setBounds(59, 10, 114, 19);
		contentPane.add(textFieldIdProd);
		textFieldIdProd.setColumns(10);
		
		JLabel lblStockEntrant = new JLabel("Stock entrant");
		lblStockEntrant.setBounds(12, 110, 107, 15);
		contentPane.add(lblStockEntrant);
		
		JLabel lblStockSortant = new JLabel("Stock sortant ");
		lblStockSortant.setBounds(273, 110, 114, 15);
		contentPane.add(lblStockSortant);
		
		textFieldStockEntrant = new JTextField();
		textFieldStockEntrant.setText("0");
		textFieldStockEntrant.setBounds(12, 137, 114, 19);
		contentPane.add(textFieldStockEntrant);
		textFieldStockEntrant.setColumns(10);
		
		textFieldStockSortant = new JTextField();
		textFieldStockSortant.setText("0");
		textFieldStockSortant.setBounds(273, 137, 114, 19);
		contentPane.add(textFieldStockSortant);
		textFieldStockSortant.setColumns(10);
		
		btnConfirmer = new JButton("Confirmer");
		btnConfirmer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnConfirmer.setName("Confirmer");
		btnConfirmer.setBounds(12, 200, 117, 25);
		contentPane.add(btnConfirmer);
		
		btnRetour = new JButton("Retour");
		btnRetour.setName("retour");
		btnRetour.setBounds(273, 200, 117, 25);
		contentPane.add(btnRetour);
		
		lblErreur = new JLabel("");
		lblErreur.setBounds(12, 56, 416, 15);
		contentPane.add(lblErreur);
		
		JLabel lblNom = new JLabel("Nom");
		lblNom.setBounds(209, 12, 70, 15);
		contentPane.add(lblNom);
		
		textFieldName = new JTextField();
		textFieldName.setBounds(297, 10, 114, 19);
		contentPane.add(textFieldName);
		textFieldName.setColumns(10);
	}
	public JButton getBtnConfirmer() {
		return btnConfirmer;
	}
	public JTextField getTextFieldStockEntrant() {
		return textFieldStockEntrant;
	}
	public JTextField getTextFieldStockSortant() {
		return textFieldStockSortant;
	}
	public JTextField getTextFieldIdProd() {
		return textFieldIdProd;
	}
	public JButton getBtnRetour() {
		return btnRetour;
	}
	public JLabel getLblErreur() {
		return lblErreur;
	}
	public JTextField getTextFieldName() {
		return textFieldName;
	}
}
