import java.util.HashSet;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import model.Produit;

public class MyDefaultModelProduit extends DefaultTableModel{
	
	private List<Produit> Produits;
	private String[]ColumnNames = {
			"id",
			"ProduitNom",
			"ProduitQuantité",
			"ProduitCategorieId"
	};
	private HashSet<Produit> modified = new HashSet<>();

	public MyDefaultModelProduit(List<Produit> Produits) {
		this.Produits = Produits;
	}
	
	HashSet<Produit> getModifiedProduit(){
		return modified;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return Produits == null ? 0 : Produits.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		Produit p = Produits.get(row);
		Object value = null;
		switch (column) {
		case 0:
			value = p.getId();
			break;
		case 1:
			value = p.getProduitNom();
			break;
		case 2:
			value = p.getProduitQuantité();
			break;
		case 3:
			value = p.getCategorie().getId();
			break;
		}
		return value;
	}

	@Override
	public void setValueAt(Object aValue, int row, int column) {
		Produit p = Produits.get(row);
		switch (column) {
		case 1:
			 p.setProduitNom((String) aValue);
			 break;

		default:
			
		}
		modified.add(p);
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		
		return ColumnNames[column];
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		// TODO Auto-generated method stub
		if (column ==0) {
			return false;
		}
		else {
		return true;
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		Class<?> type = null;
		switch (columnIndex) {
			case 0:
				type = Integer.class;
				break;
				
			case 1:
				type = String.class;
				break;
				
			case 2:
				type = Integer.class;
				break;
				
			case 3:
				type = Integer.class;
				break;
				
				
		}	
		return type;
	}
	
}
