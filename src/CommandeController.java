import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;

import DAO.DAOCategorie;
import DAO.DAOCommande;
import DAO.DAOProduit;
import View.AddProduit;
import View.AfficherCommande;
import View.AfficherProduit;
import model.Commande;
import util.HibernateUtil;


public class CommandeController implements ActionListener{
	
	private AfficherCommande fenetre;
	DAOCommande daoCommande;
	List<Commande> commandes;
	MyDefaultModelCommande myDTMC;

	public CommandeController(AfficherCommande f, DAOCommande daoCommande) {
		this.fenetre = f;
		this.daoCommande = daoCommande;	
		
		fenetre.getBtnRetour().addActionListener(this);
		
	}
	
	public void init() {
		commandes = daoCommande.findAll();
		fenetre.setVisible(true);
		myDTMC = new MyDefaultModelCommande(commandes);
		fenetre.getTable().setModel(myDTMC);
	}
	
	public void Retour() {
		fenetre.setVisible(false);
		ProduitController count = new ProduitController(new AfficherProduit(), new AddProduit(), new DAOProduit(HibernateUtil.getSessionFactory().openSession()), new DAOCategorie(HibernateUtil.getSessionFactory().openSession()));
		count.init();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton)e.getSource();
		switch (btn.getName()) {
		case "retour":
			Retour();
			break;
			
		default:
			break;
		}
	}
}
