import java.util.HashSet;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import model.Commande;

public class MyDefaultModelCommande extends DefaultTableModel{
	
	private List<Commande> commandes;
	private String[]ColumnNames = {
			"id",
			"ProduitNom",
			"ProduitId",
			"UserId"
	};
	private HashSet<Commande> modified = new HashSet<>();

	public MyDefaultModelCommande(List<Commande> commandes) {
		this.commandes = commandes;
	}
	
	HashSet<Commande> getModifiedProduit(){
		return modified;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return commandes == null ? 0 : commandes.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		Commande c = commandes.get(row);
		Object value = null;
		switch (column) {
		case 0:
			value = c.getId();
			break;
		case 1:
			value = c.getCommandeProduitNom();
			break;
		case 2:
			value = c.getProduit().getId();
			break;
		case 3:
			value = c.getUser().getId();
			break;
		}
		return value;
	}

	@Override
	public void setValueAt(Object aValue, int row, int column) {
		Commande p = commandes.get(row);
		switch (column) {

		default:
			
		}
		modified.add(p);
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		
		return ColumnNames[column];
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		// TODO Auto-generated method stub
		if (column ==0) {
			return false;
		}
		else {
		return true;
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		Class<?> type = null;
		switch (columnIndex) {
			case 0:
				type = Integer.class;
				break;
				
			case 1:
				type = String.class;
				break;
				
			case 2:
				type = Integer.class;
				break;
				
			case 3:
				type = Integer.class;
				break;
				
				
		}	
		return type;
	}
	
}
