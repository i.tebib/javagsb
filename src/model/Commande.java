package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Commande database table.
 * 
 */
@Entity
@NamedQuery(name="Commande.findAll", query="SELECT c FROM Commande c")
public class Commande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CommandeProduitNom")
	private String commandeProduitNom;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="CommandeUserId", referencedColumnName="id")
	private User user;

	//bi-directional many-to-one association to Produit
	@ManyToOne
	@JoinColumn(name="CommandeProduitid", referencedColumnName="id")
	private Produit produit;
	
	private String prodVendu;

	public Commande() {
	}

	public String getCommandeProduitNom() {
		return this.commandeProduitNom;
	}

	public void setCommandeProduitNom(String commandeProduitNom) {
		this.commandeProduitNom = commandeProduitNom;
	}

	public int getId() {
		return this.id;
	}
	
	public String getProdVendu() {
		return this.prodVendu;
	}
	
	public void setProdVendu(String prodVendu) {
		this.prodVendu = prodVendu;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Produit getProduit() {
		return this.produit;
	}

	public void setProduit(Produit produit) {
		this.produit = produit;
	}

}