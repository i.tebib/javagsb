package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Produit database table.
 * 
 */
@Entity
@NamedQuery(name="Produit.findAll", query="SELECT p FROM Produit p")
public class Produit implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name="ProduitNom")
	private String produitNom;

	@Column(name="ProduitQuantité")
	private int produitQuantité;

	//bi-directional many-to-one association to Categorie
	@ManyToOne
	@JoinColumn(name="ProduitCategorieId", referencedColumnName="id")
	private Categorie categorie;

	//bi-directional many-to-one association to Commande
	@OneToMany(mappedBy="produit")
	private List<Commande> commandes;

	public Produit() {
	}

	public String getProduitNom() {
		return this.produitNom;
	}

	public void setProduitNom(String produitNom) {
		this.produitNom = produitNom;
	}

	public int getProduitQuantité() {
		return this.produitQuantité;
	}

	public void setProduitQuantité(int produitQuantité) {
		this.produitQuantité = produitQuantité;
	}

	public Categorie getCategorie() {
		return this.categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public List<Commande> getCommandes() {
		return this.commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

	public Commande addCommande(Commande commande) {
		getCommandes().add(commande);
		commande.setProduit(this);

		return commande;
	}

	public Commande removeCommande(Commande commande) {
		getCommandes().remove(commande);
		commande.setProduit(null);

		return commande;
	}

}