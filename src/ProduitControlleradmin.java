
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import DAO.DAOCategorie;
import DAO.DAOCommande;
import DAO.DAOConnexion;
import DAO.DAOGeneric;
import DAO.DAOProduit;
import View.AddProduit;
import View.AfficherProduitsAdmin;
import model.Produit;
import util.HibernateUtil;
import model.Categorie;

public class ProduitControlleradmin implements ActionListener{

	private AfficherProduitsAdmin fenetre;
	private AddProduit addProd;
	DAOProduit daoProduit;
	DAOCategorie daoCategorie;
	List<Produit> Produits;
	MyDefaultModelProduit myDTMP;

	public ProduitControlleradmin(AfficherProduitsAdmin f, AddProduit ap, DAOProduit daoProduit, DAOCategorie daoCategorie) {
		this.fenetre = f;
		this.addProd = ap;
		this.daoProduit = daoProduit;
		this.daoCategorie = daoCategorie;
		
		fenetre.getBtnAjouter().addActionListener(this);
		
		fenetre.getBtnUpdate().addActionListener(this);
		
		fenetre.getBtnDelete().addActionListener(this);
		
		fenetre.getBtnGestionStock().addActionListener(this);
		
		fenetre.getBtnAfficherCommande().addActionListener(this);
		
		fenetre.getBtnInventaire().addActionListener(this);
		
		addProd.getBtnConfirme().addActionListener(this);
		
		addProd.getBtnRetour().addActionListener(this);
		
		
	}
	
	public void init() {
		Produits = daoProduit.findAll();
		fenetre.setVisible(true);
		myDTMP = new MyDefaultModelProduit(Produits);
		fenetre.getTable().setModel(myDTMP);
	}
	
	public void viewAjouter() {
		fenetre.setVisible(false);
		addProd.setVisible(true);
	}
	
	public void Retour() {
		addProd.setVisible(false);
		fenetre.setVisible(true);
	}

	public void Ajouter() {
		String nom = addProd.getTextFieldNomProduit().getText();
		int quantite = Integer.parseInt(addProd.getTextFieldQuantite().getText());
		int categorie = Integer.parseInt(addProd.getTextFieldCategorie().getText());
		Categorie cat = new Categorie();
		Produit prod= new Produit();
		cat = daoCategorie.find(categorie);
		prod.setCategorie(cat);
		prod.setProduitNom(nom);
		prod.setProduitQuantité(quantite);
		daoProduit.saveOrUpdate(prod);
		addProd.setVisible(false);
		init();
		
	}
	
	public void update() throws SQLException {
		HashSet<Produit> p = myDTMP.getModifiedProduit();
		Iterator<Produit> pr = p.iterator();
		 while (pr.hasNext()) {
			 Produit prod = pr.next();
	            System.out.println(prod);
	            daoProduit.saveOrUpdate(prod);
	        }
	}
	
	public void delete() throws SQLException {
		int id = fenetre.getTable().getSelectedRow();
		if(id != (-1)) {
			Produit p = Produits.get(id);
			Produits.remove(id);
            daoProduit.delete(p);
            myDTMP.fireTableDataChanged();
        }
	}
	
	public void GestionStock() {
		fenetre.setVisible(false);
		GestionStockController count = new GestionStockController(new View.GestionStock(), new DAOCommande(HibernateUtil.getSessionFactory().openSession()), new DAOProduit(HibernateUtil.getSessionFactory().openSession()), new DAOConnexion(HibernateUtil.getSessionFactory().openSession()));
		count.init();
	}
	
	public void AfficherCommande() {
		fenetre.setVisible(false);
		CommandeController count = new CommandeController(new View.AfficherCommande(), new DAOCommande(HibernateUtil.getSessionFactory().openSession()));
		count.init();
	}
	
	public void Inventaire() {
		fenetre.setVisible(false);
		InventaireController count = new InventaireController(new View.Inventaire(), new DAOCommande(HibernateUtil.getSessionFactory().openSession()));
		count.init();
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton)e.getSource();
		switch (btn.getName()) {
		case "AjouterProduit":
			viewAjouter();
			break;
			
		case "Update":
			try {
				update();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
			
		case "Delete":
			try {
				delete();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
			
		case "AfficherCommande":
			AfficherCommande();
			break;
			
		case "GestionStock":
			GestionStock();
			break;
			
		case "Confirmer":
			Ajouter();
			break;	
			
		case "inventaire":
			Inventaire();
			break;
			
		case "Retour":
			Retour();
			break;
			
		default:
			break;
		}
		
	}
}
