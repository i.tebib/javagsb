import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;

import DAO.DAOCategorie;
import DAO.DAOCommande;
import DAO.DAOConnexion;
import DAO.DAOProduit;
import model.Produit;
import model.User;
import util.HibernateUtil;
import model.Commande;
import View.AddProduit;
import View.AfficherProduit;
import View.GestionStock;

public class GestionStockController implements ActionListener{
	
	private GestionStock fenetre;
	DAOCommande daoCommande;
	DAOProduit daoProduit;
	DAOConnexion daoUser;
	
	public GestionStockController(GestionStock gf, DAOCommande daoCommande, DAOProduit daoProduit, DAOConnexion daoUser) {
		this.fenetre = gf;
		this.daoCommande = daoCommande;
		this.daoProduit = daoProduit;
		this.daoUser = daoUser;
		
		fenetre.getBtnConfirmer().addActionListener(this);
		
		fenetre.getBtnRetour().addActionListener(this);
	}
	
	public void init() {
		fenetre.setVisible(true);
	}
	
	public void Retour() {
		fenetre.setVisible(false);
		ProduitController count = new ProduitController(new AfficherProduit(), new AddProduit(), new DAOProduit(HibernateUtil.getSessionFactory().openSession()), new DAOCategorie(HibernateUtil.getSessionFactory().openSession()));
		count.init();
	}
	
	public void GererStock(){
		
		int idProd  = Integer.parseInt(fenetre.getTextFieldIdProd().getText());
		int stE = Integer.parseInt(fenetre.getTextFieldStockEntrant().getText());
		int stS = Integer.parseInt(fenetre.getTextFieldStockSortant().getText());
		Commande com = new Commande();
		Produit prod = daoProduit.find(idProd);
		if ((stE>0) && (stS>0)) {
			String erreur = "Vous ne pouvez pas entrer du stock entrant et sortant";
			fenetre.getLblErreur().setText(erreur);
		}
		if ((stE>0) && (stS==0))
		{
			stE = prod.getProduitQuantité()+stE;
			prod.setProduitQuantité(stE);
			daoProduit.saveOrUpdate(prod);
			CreateCommande(prod);
			fenetre.setVisible(false);
			ProduitController count = new ProduitController(new AfficherProduit(), new AddProduit(), new DAOProduit(HibernateUtil.getSessionFactory().openSession()), new DAOCategorie(HibernateUtil.getSessionFactory().openSession()));
			count.init();
		}
		if ((stE==0) && (stS>0))
		{
			stS = prod.getProduitQuantité()-stS;
			if (stS<0) {
				String str = String.valueOf(prod.getProduitQuantité());
				String erreur = "Vous n'avez que".concat(str);
				fenetre.getLblErreur().setText(erreur);
			}
			else {
				prod.setProduitQuantité(stS);
				daoProduit.saveOrUpdate(prod);
				CreateCommande(prod);
				fenetre.setVisible(false);
				ProduitController count = new ProduitController(new AfficherProduit(), new AddProduit(), new DAOProduit(HibernateUtil.getSessionFactory().openSession()), new DAOCategorie(HibernateUtil.getSessionFactory().openSession()));
				count.init();
			}
		}
		if ((stE<0) || (stS<0)) {
			String erreur = "Vous ne pouvez pas entrer du stock négatif";
			fenetre.getLblErreur().setText(erreur);
		}
	}
	
	public void CreateCommande(Produit prod) {
		String name = fenetre.getTextFieldName().getText();
		User user = daoUser.findByName(name);
		Commande com = new Commande();
		com.setCommandeProduitNom(prod.getProduitNom());
		com.setProduit(prod);
		com.setUser(user);
		daoCommande.saveOrUpdate(com);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton)e.getSource();
		switch (btn.getName()) {
		case "Confirmer":
			GererStock();
			break;	
			
		case "retour":
			Retour();
			break;
			
		default:
			break;
		}
		
	}

}
